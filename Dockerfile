FROM microsoft/azure-functions-python3.6

ARG ID_RSA
ARG ID_RSA_PUB

ENV host:logger:consoleLoggingMode=always
ENV AzureFunctionsJobHost__Logging__Console__IsEnabled=true

RUN mkdir /root/.ssh/

RUN echo "$ID_RSA" > /root/.ssh/id_rsa

RUN echo "$ID_RSA_PUB" > /root/.ssh/id_rsa.pub

RUN chmod 600 /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa.pub

RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

RUN pip install git+ssh://bitbucket.org/pernod-ricard/pr-python-toolbox.git
